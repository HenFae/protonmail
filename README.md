# Proton Mail: sichere E-Mails

ProtonMail ist ein kostenfreier E-Mail-Dienst, der die Nachrichten der Nutzer verschl�sselt.
Er wurde durch Jason Stockman, Andy Yen und Wei Sun, Mitarbeiter an der CERN-Forschungseinrichtung, im Jahr 2013 gegr�ndet und ist in 25 Sprachen, u. a. Deutsch, Englisch und Franz�sisch, verf�gbar.