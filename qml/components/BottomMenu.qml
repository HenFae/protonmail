import QtQuick 2.9
import Morph.Web 0.1
import Ubuntu.Components 1.3
import QtQuick.LocalStorage 2.0
import Ubuntu.Components.Popups 1.3
import Ubuntu.Content 1.1

Rectangle {
  id: bottomMenu
  z: 100000
  width: parent.width
  height: units.gu(4)
  color: "#dfd9fc"

  anchors {
    bottom: parent.bottom
  }

  /*Rectangle {
    width: parent.width
    height: units.gu(0.1)
    color: UbuntuColors.lightGrey
  }*/

  Row {
    width: parent.width
    height: parent.height-units.gu(0.1)
    anchors {
      centerIn: parent
    }

    Item {
      width: parent.width/3
      height: parent.height

      Icon {
        anchors.centerIn: parent
        width: units.gu(2.2)
        height: width
        name: "calendar"
        color: "#6856f7"
      }

      MouseArea {
        anchors.fill: parent
        onClicked: {
          webview.url = 'https://account.proton.me/login?language=en&product=calendar'
        }
      }
    }

    Item {
      width: parent.width/3
      height: parent.height

      Icon {
        anchors.centerIn: parent
        width: units.gu(2.2)
        height: width
        name: "email"
        color: "#6856f7"
      }

      MouseArea {
        anchors.fill: parent
        onClicked: {
          webview.url = 'https://mail.protonmail.com/u/0/inbox'
        }
      }
    }

    Item {
      width: parent.width/3
      height: parent.height

      Icon {
        anchors.centerIn: parent
        width: units.gu(2.2)
        height: width
        name: "folder-symbolic"
        color: "#6856f7"
      }

      MouseArea {
        anchors.fill: parent
        onClicked: {
          webview.url = 'https://account.proton.me/login?language=en&product=drive'
        }
      }
    }
  }
}
